from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup

token_oleg = "5195198181:AAFoMyXedYGy7mf7eacKgF4B6YyfJc__05w"
token_german = "5293610886:AAHi3YURabRcxekgnh8SyjL0U5sqyLSFAp0"

admins = [319471469, 550379517]
storage = MemoryStorage()
bot = Bot(token=token_oleg)
disp = Dispatcher(bot=bot, storage=storage)


class BotStates(StatesGroup):
    waiting_for_choose_program = State()
    waiting_for_input_exercise_name = State()
    waiting_for_choose_exercise_group = State()
    waiting_for_choose_exercise_name = State()
    waiting_for_switch_exercise_input_type = State()


class AdminStates(StatesGroup):
    configure_bot = State()
    configure_bot_gif_adding = State()
    configure_bot_gif_group_adding = State()
    configure_bot_gif_names_adding = State()
    configure_bot_gif_description_adding = State()