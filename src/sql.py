import itertools
import sqlite3


async def get_groups():
    db_connection = sqlite3.connect('exercises.sqlite')
    cursor = db_connection.cursor()
    cursor.execute("SELECT name FROM groups")
    groups = cursor.fetchall()
    db_connection.close()

    return groups


async def get_exercises_by_group(group):
    db_connection = sqlite3.connect('exercises.sqlite')
    cursor = db_connection.cursor()
    cursor.execute("""SELECT groups.name, p_names.name, exercises.description, exercises_file_id.file_id
                      FROM (SELECT name, exercise_id FROM possible_names GROUP BY exercise_id) AS p_names 
                      JOIN exercises ON p_names.exercise_id = exercises.id
                      JOIN groups ON exercises.group_id = groups.id
					  JOIN exercises_file_id ON exercises.id = exercises_file_id.exercise_id
                      WHERE groups.name = ?;""", (group,))
    exercises = cursor.fetchall()
    db_connection.close()

    return exercises


async def search_exercises(intended_name):
    db_connection = sqlite3.connect('exercises.sqlite')
    cursor = db_connection.cursor()
    cursor.execute("""SELECT name, description, file_id FROM possible_names 
                    JOIN exercises ON possible_names.exercise_id = exercises.id
                    JOIN exercises_file_id ON exercises_file_id.exercise_id = exercises.id
                    WHERE name LIKE ?;
                """, ('%' + intended_name + '%',))
    results = cursor.fetchall()
    db_connection.close()

    return results


async def add_possible_names(exercise_id: int, possible_names: list):
    with sqlite3.connect("exercises.sqlite") as db_connection:
        cursor = db_connection.cursor()
        many = [(name, exercise_id) for name in possible_names]
        cursor.executemany("INSERT INTO possible_names (name, exercise_id) VALUES (?, ?)", many)


async def add_gifs(exercise_id: int, gifs: list):
    with sqlite3.connect("exercises.sqlite") as db_connection:
        cursor = db_connection.cursor()
        many = [(file_id, exercise_id) for file_id in gifs]
        cursor.executemany("INSERT INTO exercises_file_id (file_id, exercise_id) VALUES (?, ?)", many)


async def add_exercise(group: str, possible_names: list, description: str, gifs: list):
    with sqlite3.connect("exercises.sqlite") as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("INSERT INTO exercises (likes, dislikes, group_id, description) "
                       "VALUES(0, 0, (SELECT id FROM groups WHERE name = ?), ?);",
                       (group[0], description[0]))
        exercise_id = cursor.lastrowid

    await add_possible_names(exercise_id, possible_names)
    await add_gifs(exercise_id, gifs)


async def delete_exercise(exercise_id: int):
    with sqlite3.connect("exercises.sqlite") as db_connection:
        cursor = db_connection.cursor()
        cursor.execute("DELETE FROM possible_names WHERE exercise_id = ?;", (exercise_id,))
        cursor.execute("DELETE FROM exercises_file_id WHERE exercise_id = ?;", (exercise_id,))
        cursor.execute("DELETE FROM exercises WHERE id = ?;", (exercise_id,))


async def file_id_adding(file_id, caption):
    connect = sqlite3.connect("exercises.sqlite")
    cursor = connect.cursor()
    cursor.execute('''SELECT exercise_id FROM possible_names WHERE name =?;''', (caption,))
    exercise_id = cursor.fetchone()

    some = list(zip(list(exercise_id), list(file_id.split())))

    cursor.executemany('''INSERT INTO exercises_file_id(exercise_id, file_id) VALUES(?,?);''', some)
    connect.commit()
    connect.close()
