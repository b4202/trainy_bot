from config import disp
from aiogram import executor
from admin_handlers import *
from user_handlers import *

if __name__ == '__main__':
    executor.start_polling(dispatcher=disp, skip_updates=True)
