from config import bot, disp, AdminStates, admins
from aiogram import types
from aiogram.dispatcher import FSMContext
import sql


states_adding = [AdminStates.configure_bot_gif_group_adding, AdminStates.configure_bot_gif_adding]
states_deleting = []


markups = {
    'main': ['Добавление гиф', 'Удаление гиф', 'Выйти из админ-меню'],
    'gif_add': ['Продолжить', 'Назад'],
    'caption_add': ['Продолжить', 'Завершить']
}


async def get_groups_markup():
    groups = await sql.get_groups()
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    for group in groups:
        markup.add(types.KeyboardButton(group[0]))
    return markup


async def add_exercise(lul: dict):
    await sql.add_exercise(lul['group'], lul['possible_names'], lul['description'], lul['gifs'])


def get_markup(mark):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    for text in markups[mark]:
        markup.add(types.KeyboardButton(text))
    return markup


@disp.message_handler(content_types=['text'], state=AdminStates.configure_bot_gif_description_adding)
async def add_names(message, state: FSMContext):
    if message.text == '/admin':
        await state.finish()
        await AdminStates.configure_bot.set()
        await admin_menu(message, state)
    else:
        if message.text != 'Завершить':
            if "description" not in await state.get_data():
                await state.update_data(description=[message.text])
                await bot.send_message(message.chat.id, 'Описание добавлено')
            else:
                async with state.proxy() as data:
                    data['description'].append(message.text)
                await bot.send_message(message.chat.id, 'Описание добавлено')
        else:
            await state.reset_state(with_data=False)
            await AdminStates.configure_bot_gif_description_adding.set()
            await add_exercise(await state.get_data())
            await admin_menu(message, state)
        print(await state.get_data())


@disp.message_handler(content_types=['text'], state=AdminStates.configure_bot_gif_names_adding)
async def add_names(message, state: FSMContext):
    if message.text == '/admin':
        await state.finish()
        await AdminStates.configure_bot.set()
        await admin_menu(message, state)
    else:
        if message.text != 'Продолжить':
            if "possible_names" not in await state.get_data():
                await state.update_data(possible_names=[message.text])
                await bot.send_message(message.chat.id, 'Название упражнения добавлено')
            else:
                async with state.proxy() as data:
                    data['possible_names'].append(message.text)
                await bot.send_message(message.chat.id, 'Название упражнения добавлено')
        else:
            await state.reset_state(with_data=False)
            await AdminStates.configure_bot_gif_description_adding.set()
            markup = types.ReplyKeyboardMarkup(True, True)
            markup.add(types.KeyboardButton(markups['caption_add'][1]))
            await bot.send_message(message.chat.id, 'Введи описание', reply_markup=markup)
        print(await state.get_data())


@disp.message_handler(content_types=['text'], state=AdminStates.configure_bot_gif_group_adding)
async def add_group(message, state: FSMContext):
    if message.text == '/admin':
        await state.finish()
        await AdminStates.configure_bot.set()
        await admin_menu(message, state)
    else:
        if "group" not in await state.get_data():
            await state.update_data(group=[message.text])
            await bot.send_message(message.chat.id, 'Группа добавлена')
            await state.reset_state(with_data=False)
            await AdminStates.configure_bot_gif_names_adding.set()
            await bot.send_message(message.chat.id, 'Введи название упражнения', reply_markup=get_markup('caption_add'))
        else:
            await bot.send_message(message.chat.id, 'Произошла ошибка, начни заново (/admin)')
            await state.finish()
            await AdminStates.configure_bot.set()
    print(await state.get_data())

@disp.message_handler(content_types=['text'], state=states_adding)
async def gif_adding_states_set(message, state: FSMContext):
    if message.text == '/admin':
        await state.finish()
        await AdminStates.configure_bot.set()
        await admin_menu(message, state)
    else:
        if message.text == markups['gif_add'][0]: #Продолжить
            await state.reset_state(with_data=False)
            await AdminStates.configure_bot_gif_group_adding.set()
            print(await state.get_state())
            await bot.send_message(message.chat.id, 'Выбери группу', reply_markup=await get_groups_markup())

        elif message.text == markups['gif_add'][1]: #Назад
            await state.finish()
            await AdminStates.configure_bot.set()
            print(await state.get_state())
            await admin_menu(message, state)

        elif message.text == markups['main'][2]: #Выйти из админ-меню
            print(await state.get_state())
            await state.finish()
            print(await state.get_state())
            await bot.send_message(message.chat.id, 'Напиши /start, /help для начала работы')

        elif message.text == markups['caption_add'][0]:
            await state.finish()
            await AdminStates.configure_bot.set()
            await admin_menu(message, state)

        elif message.text == markups['caption_add'][1]: #Добавить еще гиф
            await state.reset_state(with_data=False)
            await AdminStates.configure_bot_gif_adding.set()
            await bot.send_message(message.chat.id, 'Догружай гифки', reply_markup=get_markup('caption_add'))


@disp.message_handler(content_types=['text'], state=AdminStates.configure_bot)
async def gif_edit_selection(message, state: FSMContext):
    if message.text == '/admin':
        await state.finish()
        await AdminStates.configure_bot.set()
        await admin_menu(message, state)

    else:
        if message.text == markups['main'][0]:  # Добавление гиф
            await state.finish()
            await AdminStates.configure_bot_gif_adding.set()
            print(await state.get_state())
            await bot.send_message(message.chat.id, 'Загружай гифки', reply_markup=get_markup('gif_add'))

        elif message.text == markups['main'][1]:
            await bot.send_message(message.chat.id, text= 'Пока в разработке..zzz..')

        elif message.text == markups['main'][2]:  # Выйти из админ-меню
            print(await state.get_state())
            await state.finish()
            print(await state.get_state())
            await bot.send_message(message.chat.id, 'Админские права сброшены')


@disp.message_handler(commands=['admin'], state="*")
async def admin_menu(message, state: FSMContext):
    if message.chat.id in admins:
        await state.finish()
        await AdminStates.configure_bot.set()
        await bot.send_message(message.chat.id, "Ты в админ-меню", reply_markup=get_markup('main'))

    else:
        await bot.send_message(chat_id=message.chat.id,
                               text="У вас нет доступа к режиму редактирования")


@disp.message_handler(content_types=['animation'], state=AdminStates.configure_bot_gif_adding)
async def scan_animation(message: types.Message, state: FSMContext):
    document_id = message.animation.file_id
    file_info = await bot.get_file(document_id)
    if not await state.get_data():
        await state.update_data(gifs=[file_info.file_id])
    else:
        async with state.proxy() as data: #Это один из вариантов реализации. Этот проще.
            data['gifs'].append(file_info.file_id)
    print(await state.get_data())
    await message.reply(text=f'file_id:\n{file_info.file_id}')
