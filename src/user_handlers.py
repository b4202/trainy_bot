from config import bot, disp, BotStates
from aiogram.dispatcher import FSMContext
from aiogram import types
import sql
import aiogram.utils.markdown as fmt

markups = {
    'main': [('Техника выполнения упражнений', 'technics')],
    'technics': [('Ручной ввод', 'technics_manual_input'), ('Выбор по группам', 'inline_group_choice')],
    'no_exercises_founded': [('Назад', 'technics')],
    'sport_calc': [("Индекс массы тела", 'bmi'),
                   ("Калькулятор рекомендуемого суточного потребления калорий", 'calories_calc'),
                   ('В главное меню', 'main')],
    'sex': [('Мужской', 'male'), ('Женский', 'female')],
    'activity': [('Минимальная', 1.2), ('Легкая', 1.35), ('Средняя', 1.55), ('Высокая', 1.75),
                 ('Экстремально высокая', 1.95)]
}

titles = {'main': 'Привет, я твой спортивный ассистент!\nЧем могу быть полезен?',
          'sport_calc': 'Выбери калькулятор',
          'technics': 'Удобнее ввести вручную или хочешь продолжить с кнопками?',
          'sex': 'Выбери свой пол',
          'activity': 'Твоя повседневная активность\n'}


def get_markup_for(section):
    markup = types.InlineKeyboardMarkup()
    for text, callback_data in markups[section]:
        markup.add(types.InlineKeyboardButton(text, callback_data=callback_data))
    return markup


@disp.message_handler(commands=['start'], state="*")
async def main_menu(message, state: FSMContext):
    await state.finish()
    await bot.send_message(message.chat.id,
                           text=titles['main'],
                           reply_markup=get_markup_for('main'))
    await BotStates.waiting_for_choose_program.set()


@disp.message_handler(commands=['help'], state="*")
async def help(message):
    await bot.send_animation(message.chat.id, "CgACAgIAAxkBAAIGZ2JA_0iED7dVoy8SNy7Jw0iGu4M0AAIfFwACu80JSkKHWo5ZERhzIwQ",
                       caption="Создатели бота: @ajourmany👳🏾‍♂️, @srvxsIVDd🧑🏼‍🦱. "
                                      "\n🦾Я помогу тебе разобраться в технике выполнения упражнений."
                               "\n⬇️Используй кнопки для навигации⬇️")



@disp.callback_query_handler(lambda call: call.data == 'technics', state="*")
async def technics_handler(call, state: FSMContext):
    await call.answer()
    await bot.edit_message_text(titles['technics'],
                                call.message.chat.id,
                                call.message.message_id,
                                reply_markup=get_markup_for('technics'))
    await BotStates.waiting_for_switch_exercise_input_type.set()


@disp.callback_query_handler(state=BotStates.waiting_for_switch_exercise_input_type)
async def choose_technics_input_type_handler(call, state: FSMContext):
    await call.answer()
    if call.data == 'technics_manual_input':
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton("Назад", callback_data='technics'))
        await bot.edit_message_text(chat_id=call.message.chat.id,
                                    text="Введи название упражнения",
                                    message_id=call.message.message_id,
                                    reply_markup=markup)
        await BotStates.waiting_for_input_exercise_name.set()
    else:
        await BotStates.waiting_for_choose_exercise_group.set()
        await choose_group_handler(call, state)


@disp.callback_query_handler(state=BotStates.waiting_for_choose_exercise_group)
async def choose_group_handler(call, state: FSMContext):
    await call.answer()
    if call.data in ['back', 'next', 'inline_group_choice']:
        groups = await sql.get_groups()
        markup = types.InlineKeyboardMarkup()
        if call.data == 'next':
            for group in groups[3:]:
                markup.add(types.InlineKeyboardButton(group[0], callback_data=group[0]))
                navigate = types.InlineKeyboardButton(text='<', callback_data='back')
        elif call.data == 'back' or call.data == 'inline_group_choice':
            for group in groups[:3]:
                markup.add(types.InlineKeyboardButton(group[0], callback_data=group[0]))
                navigate = types.InlineKeyboardButton(text='>', callback_data='next')
        back = types.InlineKeyboardButton(text='Назад', callback_data='technics')
        markup.row(back, navigate)

        if call.message.animation:
            await bot.delete_message(call.message.chat.id,
                                     call.message.message_id)

            await bot.send_message(call.message.chat.id,
                                   text='Выбери группу мышц',
                                   reply_markup=markup)
        else:
            await bot.edit_message_text('Выбери группу мышц',
                                        call.message.chat.id,
                                        call.message.message_id,
                                        reply_markup=markup)
    else:
        await BotStates.waiting_for_choose_exercise_name.set()
        await state.update_data(group=call.data, ix=0, gifs=1)
        await choose_exercise_handler(call, state)


@disp.callback_query_handler(state=BotStates.waiting_for_choose_exercise_name)
async def choose_exercise_handler(call, state: FSMContext):
    await call.answer()
    if call.data == 'inline_group_choice':
        await state.finish()
        await BotStates.waiting_for_choose_exercise_group.set()
        await choose_group_handler(call, state)
    else:
        state_data = await state.get_data()
        exercises = await sql.get_exercises_by_group(state_data['group'])
        exercises_ix = state_data['ix']
        markup = types.InlineKeyboardMarkup()
        if call.data == 'next':
            if exercises_ix + 1 == len(exercises):
                exercises_ix = 0
            else:
                exercises_ix += 1
        elif call.data == 'back':
            if exercises_ix - 1 == -1:
                exercises_ix = len(exercises) - 1
            else:
                exercises_ix -= 1

        await state.update_data(group=state_data['group'], ix=exercises_ix)
        print(exercises[exercises_ix])

        back = types.InlineKeyboardButton(text='Назад', callback_data='inline_group_choice')
        prev = types.InlineKeyboardButton(text='<', callback_data='back')
        next = types.InlineKeyboardButton(text='>', callback_data='next')
        markup.add(prev, next)
        markup.add(back)
        if call.message.animation:
            await bot.edit_message_media(chat_id=call.message.chat.id,
                                         message_id=call.message.message_id,
                                         media=types.InputMediaAnimation(
                                             media=exercises[exercises_ix][3],
                                             caption=exercises[exercises_ix][2]),
                                         reply_markup=markup)
        else:
            await bot.delete_message(chat_id=call.message.chat.id,
                                     message_id=call.message.message_id)
            await bot.send_animation(chat_id=call.message.chat.id,
                                     animation=exercises[exercises_ix][3],
                                     caption=exercises[exercises_ix][2],
                                     reply_markup=markup)


@disp.message_handler(state=BotStates.waiting_for_input_exercise_name)
async def search_exercise(message: types.Message, state: FSMContext):
    results = await sql.search_exercises(message.text)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Назад", callback_data='technics'))
    if len(results) == 0:
        await bot.send_message(message.chat.id,
                               text="Этого упражнения нет в базе данных\nПопробуй ещё раз",
                               reply_markup=markup)
    elif len(results) == 1:
        print(results[0])
        await bot.send_animation(chat_id=message.chat.id,
                                 caption=fmt.text(fmt.hbold(results[0][0]),
                                                  fmt.text(results[0][1]),
                                                  sep="\n"),
                                 parse_mode="HTML",
                                 animation=results[0][2],
                                 reply_markup=markup)

    else:
        reply = "Найдено несколько совпадений:\n"
        for result in results:
            reply += result[0] + '\n'
        reply += "\nУточни, что ты ищешь"
        send = await bot.send_message(message.chat.id, reply)


@disp.callback_query_handler(lambda call: call.data in markups.keys())
async def callback_handler(call):
    await bot.edit_message_text(titles[call.data],
                                call.message.chat.id,
                                call.message.message_id,
                                reply_markup=get_markup_for(call.data))
